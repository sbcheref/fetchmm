const express = require('express');
const emailProvider = require('./src/index'); // Adjust the path if necessary
const cors = require('cors');

const app = express();

// Enable CORS for all routes
app.use(cors());

app.get('/api/:address', async (req, res) => {
    const { address } = req.params;

    try {
        const service = await emailProvider.get(address);
        res.json(service);
    } catch (error) {
        res.status(500).json({ error: 'Email provider was not reached' });
    }
});

module.exports = app;


app.listen(5000, () => {
    console.log('Server is running on port 5000');
});
